#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PDitto, a multibuffer copy-paste extension for X.

Copyright (c) 2012-2015 Indrek Sünter

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
"""

__author__ = "Indrek Sünter";
__version__ = "1.1";

import os;
import sys;
import optparse;

from pd_hotkey import PDHotkeys;

class PDitto:
	def __init__( self ):
		# Application path for this script
		self.dir_app = sys.path[ 0 ];
		# Directory for storing clipboard buffers
		self.dir_buffers = os.environ[ 'HOME' ] + '/.pditto/buffers/';
		# Hotkeys
		self.hotkeys = PDHotkeys();
	
	"""
	Gets clip buffer filepath, based on clipboard buffer index.
	
	Arguments:
	buf_id -- Index of the clip buffer
	"""
	def get_clip_buf_path( self, buf_id ):
		path = self.dir_buffers + 'cb' + str( buf_id );
		return path;
	
	"""
	Makes a system call to copy the active selection into
	a clipboard buffer.
	
	Arguments:
	buf_id -- Index of the clip buffer to copy the selection to
	"""
	def call_clip_to_buf( self, buf_id ):
		try:
			# Make sure the supplied buffer index is within 0 - 9.
			buf_id = int( buf_id );
			if buf_id < 0 or buf_id > 9:
				raise ValueError('')
			
			# Direct the output of current selection to the 
			# clipboard buffer file
			cmd = 'xsel -b > ' + self.get_clip_buf_path( buf_id );
			# Make the system call
			os.system( cmd );
		except (TypeError, ValueError) as e:
			print 'Error: Clipboard buffer index must be an integer in the range of 0 - 9.';

	"""
	Makes a system call to copy the contents of a clipboard buffer
	to the actual clipboard.
	
	Arguments:
	buf_id -- Index of the clip buffer to copy from
	"""
	def call_buf_to_clip( self, buf_id ):
		try:
			# Make sure the supplied buffer index is within 0 - 9.
			buf_id = int( buf_id );
			if buf_id < 0 or buf_id > 9:
				raise ValueError('')
		
			# Get the path to the requested clipboard buffer file
			bufpath = self.get_clip_buf_path( buf_id );
			# Only attempt to read it if it exists
			if os.path.exists( bufpath ):
				# Direct the output of buffer to clipboard
				cmd = 'cat ' + bufpath + '| xsel -b -i';
				# Make the system call
				os.system( cmd );
		except (TypeError, ValueError) as e:
			print 'Error: Clipboard buffer index must be an integer in the range of 0 - 9.';
	
	"""
	Run the script.
	"""
	def run( self ):
		# Make sure the buffers directory exists
		if not os.path.exists( self.dir_buffers ):
			os.makedirs( self.dir_buffers );
		
		ver_str = '%prog ' + __version__ + '\nCopyright (C) 2015 ' + __author__ + '\nDistributed under the terms of the MIT License.';
		usage_str = 'Usage: %prog [OPTIONS]\nMultibuffer copy-paste extension for X.'
		
		p = optparse.OptionParser(usage=usage_str, version=ver_str);
		pgio = optparse.OptionGroup(p, 'Operations on stored buffers', 'Copying between stored buffers and desktop manager clipboard.')
		pgio.add_option('-i', '--in', dest='mode_in', action='store', default='', help='Copy the selection into the clipboard buffer specified by index within range 0 - 9.');
		pgio.add_option('-o', '--out', dest='mode_out', action='store', default='', help='Copy from the clipboard buffer, which is specified by index within range 0 - 9.');
		p.add_option_group(pgio)
		
		pghk = optparse.OptionGroup(p, 'Hotkey options', 'Configuring and generating desktop manager hotkeys.')
		pghk.add_option('-S', '--key-store', dest='hotkey_store', action='store', default='<Primary><Shift>', help='Sets key combination for storing clipboard buffer, for example: "<Primary><Shift>".');
		pghk.add_option('-E', '--key-emerge', dest='hotkey_emerge', action='store', default='<Primary>', help='Sets key combination for retrieving clipboard buffer, for example: "<Primary>".');
		pghk.add_option('-I', '--inst-keys', dest='install_keys', action='store_true', 
			   help='Installs hotkeys to desktop manager user configuration. Please use in combination with the (-S, --key-store) or (-E, --key-emerge) options.');
		pghk.add_option('-U', '--uninst-keys', dest='uninstall_keys', action='store_true', help='Uninstalls pditto hotkeys from desktop manager user configuration.');
		pghk.add_option('-1', '--xfce-shift-map', dest='xfce_shift_map', action='store_true', default=False, 
			   help='In XFCE4 there are issues with key combinations containing Shift. This flag applies a naive workaround.');
		p.add_option_group(pghk);
		
		options, arguments = p.parse_args()
		
		# Interpret input, output modes.
		if options.mode_in != '' and options.mode_out != '':
			p.error("Options (-i, --in) and (-o, --out) are mutually exclusive.");
		elif options.mode_in != '':
			self.call_clip_to_buf( options.mode_in );
		elif options.mode_out != '':
			self.call_buf_to_clip( options.mode_out );
		
		# Interpret installation, uninstallation modes.
		if options.install_keys and options.uninstall_keys:
			p.error("Options (-I, --inst-keys) and (-U, --uninst-keys) are mutually exclusive.");
		elif options.install_keys:
			self.hotkeys.set_xfce_shift_map( options.xfce_shift_map );
			self.hotkeys.generate_hotkeys( options.hotkey_store, 'pditto -i ', options.hotkey_emerge, 'pditto -o ' );
			self.hotkeys.install_xfce_xml();
		elif options.uninstall_keys:
			self.hotkeys.uninstall_xfce_xml();

def main():
	pd = PDitto();
	pd.run();

if __name__ == "__main__":
	main();

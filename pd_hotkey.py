# -*- coding: utf-8 -*-

"""
PDitto, a multibuffer copy-paste extension for X.

Copyright (c) 2012-2015 Indrek Sünter

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
"""

import os;
import copy;
import xml.dom.minidom;

class PDHotkeys:
	# A watermark to recognize auto-installed hotkeys
	watermark = ' #pditto';
	# TODO:: Figure out a better way to work around shift-maps. This probably doesn't work on all keyboard layouts.
	xfce_shift_map = {
		'0':'equal', 
		'1':'exclam',
		'2':'quotedbl',
		'3':'numbersign',
		'4':'currency',
		'5':'percent',
		'6':'ampersand',
		'7':'slash',
		'8':'parenleft',
		'9':'parenright'}

	"""
	A single key combination with an assignment
	"""
	class Hotkey:
		# Key combination
		combination = '';
		# Command to be executed
		command = '';
	
	def __init__( self ):
		# Get the name of the current desktop manager
		self.deskman = os.environ['XDG_CURRENT_DESKTOP'];
		# Initialize, based on the desktop manager
		self.init_deskman();
		# No hotkeys at first
		self.hotkeys = [];
		# Work-around for XFCE bug in Shift + Number combinations
		self.use_xfce_shift_map = False

	"""
	Desktop manager specific initialization
	"""
	def init_deskman( self ):
		if self.deskman == 'XFCE':
			self.cfg_file = os.environ[ 'HOME' ] + '/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml';
		elif self.deskman == 'KDE':
			self.cfg_file = os.environ[ 'HOME' ] + '/.kde/share/config/khotkeysrc';
		else:
			self.cfg_file = None;
	
	def set_xfce_shift_map( self, enabled ):
		self.use_xfce_shift_map = enabled
	
	"""
	Generates a list of hotkeys for PDitto buffers
	"""
	def generate_hotkeys( self, in_keys, in_cmd, out_keys, out_cmd ):
		hk = PDHotkeys.Hotkey();
		
		# Hotkeys for copying
		for i in range( 10 ):
			key = str( i )
			# Shift-remap the numbers, if requested.
			if self.use_xfce_shift_map and 'Shift' in in_keys:
				key = PDHotkeys.xfce_shift_map[ key ]
			
			hk.combination = in_keys + key;
			hk.command = in_cmd + str( i ) + PDHotkeys.watermark;
			# Add it
			self.hotkeys.append( copy.copy( hk ) );
		
		# Hotkeys for pasting
		for i in range( 10 ):
			key = str( i )
			# Shift-remap the numbers, if requested.
			if self.use_xfce_shift_map and 'Shift' in out_keys:
				key = PDHotkeys.xfce_shift_map[ key ]
			
			hk.combination = out_keys + key;
			hk.command = out_cmd + str( i ) + PDHotkeys.watermark;
			# Add it
			self.hotkeys.append( copy.copy( hk ) );

	"""
	Removes empty lines from the text
	"""
	def strip_empty_lines( self, text ):
		new_text = '';
		for line in text.split('\n'):
			if line.strip():
				new_text += line + '\n'
		return new_text;

	"""
	Installs hotkey combinations for Xfce XML configuration
	"""
	def install_xfce_xml( self ):
		# Parse the XML
		self.cfg_tree = xml.dom.minidom.parse( self.cfg_file );
		
		# Get all channels
		channels = self.cfg_tree.getElementsByTagName( 'channel' );
		# Find the keyboard shortcuts channel
		for channel in channels:
			if channel.getAttribute( 'name' ) == 'xfce4-keyboard-shortcuts':
				# Get all properties
				properties = channel.getElementsByTagName( 'property' );
				# Find the commands property
				for property in properties:
					if property.getAttribute( 'name' ) == 'commands':
						# Get all subproperties
						subproperties = property.getElementsByTagName( 'property' );
						# Find the custom property
						for subproperty in subproperties:
							if subproperty.getAttribute( 'name' ) == 'custom':
								# Create the new properties for the hotkeys
								for hotkey in self.hotkeys:
									hkproperty = self.cfg_tree.createElement( 'property' );
									# Set attributes
									print 'Adding combination ' + hotkey.combination;
									hkproperty.setAttribute( 'name', hotkey.combination );
									hkproperty.setAttribute( 'type', 'string' );
									hkproperty.setAttribute( 'value', hotkey.command );
									# Append it
									subproperty.appendChild( hkproperty );
		# Convert the tree to an XML string
		output = self.cfg_tree.toprettyxml( encoding = 'utf-8', indent = '  ', newl = '\n' );
		# Strips empty lines (toprettyxml) generates a lot of them everywhere
		output = self.strip_empty_lines( output );
		# Write it out
		file = open( self.cfg_file, 'w+' );
		file.write( output );

	"""
	Uninstalls hotkey combinations from Xfce XML configuration
	"""
	def uninstall_xfce_xml( self ):
		# Parse the XML
		self.cfg_tree = xml.dom.minidom.parse( self.cfg_file );
		
		# Get all properties
		properties = self.cfg_tree.getElementsByTagName( 'property' );
		# Find the keyboard shortcuts channel
		for property in properties:
			# It has the watermark?
			if property.getAttribute( 'value' ).rfind( PDHotkeys.watermark ) != -1:
				print 'Removing combination ' + property.getAttribute( 'name' );
				property.parentNode.removeChild( property );
		# Convert the tree to an XML string
		output = self.cfg_tree.toprettyxml( encoding = 'utf-8', indent = '  ', newl = '\n' );
		# Strips empty lines (toprettyxml) generates a lot of them everywhere
		output = self.strip_empty_lines( output );
		# Write it out
		file = open( self.cfg_file, 'w+' );
		file.write( output );

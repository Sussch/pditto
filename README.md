# Multibuffer copy-paste extension for X

PDitto allows one to copy into multiple buffers and paste from multiple buffers,
with the help of [xsel][1].

The utility provides support for 10 clipboard buffers, which are stored in
`~/.pditto/buffers/cbX` where X is clipboard buffer index from 0 to 9.

Please do not store sensitive information in the buffers.

## Command-line options

> ~$ pditto -h
> Usage: pditto [OPTIONS]
> Multibuffer copy-paste extension for X.
> 
> Options:
>   --version             show program's version number and exit
>   -h, --help            show this help message and exit
> 
>   Operations on stored buffers:
>     Copying between stored buffers and desktop manager clipboard.
> 
>     -i MODE_IN, --in=MODE_IN
>                         Copy the selection into the clipboard buffer specified
>                         by index within range 0 - 9.
>     -o MODE_OUT, --out=MODE_OUT
>                         Copy from the clipboard buffer, which is specified by
>                         index within range 0 - 9.
> 
>   Hotkey options:
>     Configuring and generating desktop manager hotkeys.
> 
>     -S HOTKEY_STORE, --key-store=HOTKEY_STORE
>                         Sets key combination for storing clipboard buffer, for
>                         example: "<Primary><Shift>".
>     -E HOTKEY_EMERGE, --key-emerge=HOTKEY_EMERGE
>                         Sets key combination for retrieving clipboard buffer,
>                         for example: "<Primary>".
>     -I, --inst-keys     Installs hotkeys to desktop manager user
>                         configuration. Please use in combination with the (-S,
>                         --key-store) or (-E, --key-emerge) options.
>     -U, --uninst-keys   Uninstalls pditto hotkeys from desktop manager user
>                         configuration.
>     -1, --xfce-shift-map
>                         In XFCE4 there are issues with key combinations
>                         containing Shift. This flag applies a naive
>                         workaround.

Use `killall xfconfd` to apply any changes in hotkeys.


## Command-line examples

> pditto -I -1 -S '<Primary><Shift>' -E '<Primary>'

The above example installs XFCE4 hotkeys for the following:

1. *Ctrl* + *Shift* + **Number** stores clipboard contents in the buffer that corresponds to the number.
2. *Ctrl* + **Number** retrieves clipboard contents from the buffer that corresponds to the number.

> pditto -U

The above example uninstalls any hotkeys that PDitto has previously installed.
Note that PDitto uses the `#pditto` suffix to identify its own entries.

> pditto -i 4

The above example loads the contents of buffer 4 into the clipboard for pasting.

> pditto -o 0

The above example stores clipboard contents in buffer 0.

## License
PDitto is distributed under the [MIT license][2].

[1]: http://www.vergenet.net/~conrad/software/xsel/
[2]: http://opensource.org/licenses/mit-license.php